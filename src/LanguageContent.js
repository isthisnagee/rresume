import React from "react";
import { SectionHeadline } from "./ResumeUtils.js";

export default LanguageContent;

function LanguageContent({ languages, ...props }) {
  if (!languages) return false;
  const languageContent = languages.map((language, id) => {
    return <Language key={id} language={language} />;
  });
  return (
    <div>
      {SectionHeadline("Languages")}
      <ul>
        {languageContent}
      </ul>
    </div>
  );
}

function Language({ language, ...props }) {
  const valid = language && language.language && language.fluency;
  if (!valid) return false;
  return <li>{language.language}, {language.fluency}</li>;
}
