import React from "react";

export { SectionHeadline, ResumeDate, SingleResumeDate };

function ResumeDate({ startDate, endDate }) {
  // a resume date is a 1x1 grid
  const dateFormat = /\d{4}-\d{2}-\d{2}/;
  const valid = startDate && startDate.match(dateFormat);

  if (!valid) return false;
  const hasEnd = endDate && endDate.match(dateFormat);

  // we expect the date format to be like 2017-04-12
  const getMMMYY = yyyymmdd => {
    const yyyymmddNum = yyyymmdd.split("-").map(mdy => +mdy);
    const date = new Date(...yyyymmddNum);
    const formatted = date
      .toDateString()
      .split(" ")
      .filter((_, i) => !((i + 1) % 2))
      .join(" ");
    return formatted;
  };
  return (
    <div className="flx flx-aln-c tc tl-ns">
      <p>
        {`${getMMMYY(startDate)} - ${hasEnd ? getMMMYY(endDate) : "current"}`}
      </p>
    </div>
  );
}

function SingleResumeDate(date) {
  const dateFormat = /\d{4}-\d{2}-\d{2}/;
  const valid = date && date.match(dateFormat);

  if (!valid) return false;

  // we expect the date format to be like 2017-04-12
  const getMMMYY = yyyymmdd => {
    const yyyymmddNum = yyyymmdd.split("-").map(mdy => +mdy);
    const date = new Date(...yyyymmddNum);
    const formatted = date
      .toDateString()
      .split(" ")
      .filter((_, i) => !((i + 1) % 2))
      .join(" ");
    return formatted;
  };
  return (
    <div className="ResumeDate">
      <p className="ResumeDate__text">
        {getMMMYY(date)}
      </p>
    </div>
  );
}

function SectionHeadline(headline) {
  return (
    <div className="SectionHeadlineContainer">
      <div className="SectionHeadline">
        <div className="SectionHeadline__content"><h2>{headline}</h2></div>
      </div>
      <a href="#top" className="SectionHeadline__toTop" />
    </div>
  );
}
