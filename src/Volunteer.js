import React from "react";
import { Work } from "./Work.js";
import { SectionHeadline } from "./ResumeUtils.js";

function VolunteerContent({ experience, ...props }) {
  const volunteerAsWork = experience.map(exp => {
    return {
      company: exp.organization,
      position: exp.position,
      website: exp.website,
      startDate: exp.startDate,
      endDate: exp.endDate,
      summary: exp.summary,
      highlights: exp.highlights
    };
  });
  const volunteerExperience = volunteerAsWork.map((volunteer, id) => {
    return <Work key={id} job={volunteer} />;
  });
  return (
    <div>
      {SectionHeadline("Volunteer Experience")}
      {volunteerExperience}
    </div>
  );
}

export default VolunteerContent;
