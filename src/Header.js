import React from "react";
import Remarkable from "remarkable";
import RemarkableReactRenderer from "remarkable-react";
import "./Resume.css";
import "./tachyons.css";

const md = new Remarkable();
md.renderer = new RemarkableReactRenderer();

function Header({ basics, ...props }) {
  // verify that the "basics" exist
  const valid = basics && basics.name && basics.email;
  // react won't render an component if it returns `false`
  if (!valid) return false;

  const label = <HeaderLabel label={basics.label} />;
  const picture = <HeaderPicture img={basics.picture} />;
  const phone = <HeaderPhone number={basics.phone} />;
  const website = <HeaderWebsite url={basics.website} />;
  const summary = <HeaderSummary markdown={basics.summary} />;
  const location = <HeaderLocation loc={basics.location} />;
  const profiles = <HeaderProfiles profiles={basics.profiles} />;
  // maybe change the above to a list?
  return (
    <div className="Header">
      <h1 className="Header-name">
        {basics.name}
      </h1>
      <div>{label}</div>
      {picture}
      <address className="Header-contact">
        {basics.email}<br />
        {phone}<br />
        {website}
        {location}
        {profiles}
      </address>
      {summary}
    </div>
  );
}

function HeaderLabel({ label, ...props }) {
  if (!label) return false;
  return <p className="Header-label">{label}</p>;
}
function HeaderPicture({ img, ...props }) {
  if (!img) return false;
  return <img className="Header-img" src={img} alt="header logo" />;
}
function HeaderPhone({ number, ...props }) {
  if (!number) return false;
  const onlyNumbers = number.replace(/[^0-9]/, "");
  return <a className="Header-phone" href={`tel:${onlyNumbers}`}>{number}</a>;
}
function HeaderWebsite({ url, ...props }) {
  if (!url) return false;
  return <a className="Header-website" href={url}>{url}</a>;
}
function HeaderSummary({ markdown, ...props }) {
  if (!markdown) return false;
  const markedMarkdown = md.render(markdown);
  return <div className="Header_summary">{markedMarkdown}</div>;
}
function HeaderLocation({ location, ...props }) {
  if (!location) return false;
  return <p className="Header-location">{location}</p>;
}
function HeaderProfiles({ profiles, ...props }) {
  if (!profiles) return false;
  const profileList = profiles.map(({ network, url, username }, id) => {
    return (
      <li className="Header-profile__li" key={id}>
        On {network} at <a href={url}>{username}</a>
      </li>
    );
  });
  return <ul className="Header-profile__ul">You can find me: {profileList}</ul>;
}

export default Header;
