import React from "react";
import { SectionHeadline } from "./ResumeUtils.js";

function AwardContent({ awards, ...props }) {
  if (!awards) return false;
  const awardContent = awards.map((award, id) => {
    return <Award key={id} award={award} />;
  });
  return (
    <div>
      {SectionHeadline("Awards")}
      {awardContent}
    </div>
  );
}

/* 
 * Each award must follow this structure:
 * title: "Digital Compression Pioneer Award",
 * date: "2014-11-01",
 * awarder: "Techcrunch",
 * summary (opt): "There is no spoon."
 */
function Award({ award, ...props }) {
  const valid = award && award.title && award.date && award.awarder;
  if (!valid) return false;

  const summary = award.summary ? <small>{award.summary}</small> : false;
  return (
    <div>
      <em>{award.date}: {award.title} - {award.awarder}</em><br />
      {summary}
    </div>
  );
}

export default AwardContent;
