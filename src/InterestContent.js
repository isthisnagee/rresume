import React from "react";
import { SectionHeadline } from "./ResumeUtils.js";

export default InterestContent;

function InterestContent({ interests, ...props }) {
  if (!interests) return false;
  const interestContent = interests.map((interest, id) => {
    const valid = interest && interest.name && interest.keywords;
    if (!valid) return false;
    return <li>{interest.name} : {interest.keywords.join(", ")}</li>;
  });
  return (
    <div>
      {SectionHeadline("Interestes")}
      <ul>
        {interestContent}
      </ul>
    </div>
  );
}
