import React from "react";
import { SectionHeadline, ResumeDate } from "./ResumeUtils.js";

/*
 * `experience` is a list of objects of the following structure:
 *   - company # Pied Piper
 *   - position # CEO
 *   - website (opt) # https://piedpiper.com
 *   - startDate  # 2017-04-13
 *   - endDate (opt) # 2017-05-13
 *   - summary # a short statement about the company/job
 *   - highlights (opt, list of)
 *       - things that
 *       - you did at
 *       - this company/position
 */
function WorkContent({ experience, ...props }) {
  if (experience.length < 1) return false;
  const experiences = experience.map((job, id) => {
    return <Work job={job} key={id} />;
  });
  return (
    <div>
      {SectionHeadline("Work Experience")}
      {experiences}
    </div>
  );
}

function Work({ job, ...props }) {
  const valid =
    job &&
    job.company &&
    job.position &&
    job.startDate &&
    job.startDate.match(/\d{4}-\d{2}-\d{2}/) &&
    job.summary;
  if (!valid) return false;

  const website = job.website ? <a href={job.website}>{job.website}</a> : false;
  const highlights = job.highlights
    ? job.highlights.map((highlight, id) => {
        return <li key={id}>{highlight}</li>;
      })
    : false;
  return (
    <div>
      <div>
        <h3 className="f4 tc tl-ns">{job.position}, {job.company}</h3>
        <div className="tc tl-ns">{website}</div>
        <p className="lh-copy measure pl2">{job.summary}</p>
        <ul>
          {highlights}
        </ul>
      </div>
      <ResumeDate startDate={job.startDate} endDate={job.endDate} />
    </div>
  );
}

export default WorkContent;
export { Work };
