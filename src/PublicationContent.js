import React from "react";
import Remarkable from "remarkable";
import RemarkableReactRenderer from "remarkable-react";
import { SectionHeadline } from "./ResumeUtils.js";

export default PublicationContent;

function PublicationContent({ publications, ...props }) {
  if (!publications) return false;

  const publicationContent = publications.map((publication, id) => {
    return <Publication key={id} publication={publication} />;
  });
  return (
    <div>
      {SectionHeadline("Publications")}
      {publicationContent}
    </div>
  );
}

/*
 * A publication contains the following data:
 * name: "Video compression for 3d media",
 * publisher: "Hooli",
 * releaseDate: "2014-10-01",
 * website (opt): "https://en.wikipedia.org/wiki/Silicon_Valley_(TV_series)",
 * summary: some markdown 
 */
function Publication({ publication, ...props }) {
  const valid =
    publication &&
    publication.name &&
    publication.publisher &&
    publication.releaseDate &&
    publication.summary;

  if (!valid) return false;
  const md = new Remarkable();
  md.renderer = new RemarkableReactRenderer();
  const website = <a href={publication.website}>link</a>;
  return (
    <div>
      <p>
        <em>{publication.name}</em>
        {" "}
        <small>{publication.publisher}, {publication.releaseDate}</small>
      </p>
      <p>
        {website}<br />
        <small>
          {md.render(publication.summary)}
        </small>
      </p>
    </div>
  );
}
