import React from "react";
import { SectionHeadline } from "./ResumeUtils.js";

function SkillContent({ skills, ...props }) {
  if (!skills) return false;
  const skillContent = skills.map((skill, id) => {
    return <Skill key={id} skill={skill} />;
  });
  return (
    <div>
      {SectionHeadline("Skills")}
      {skillContent}
    </div>
  );
}

function Skill({ skill, ...props }) {
  const valid = skill && skill.name && skill.level && skill.keywords;
  if (!valid) return false;
  return (
    <div>
      {skill.name}, {skill.level} <small>{skill.keywords.join(", ")}</small>
    </div>
  );
}

export default SkillContent;
