import React from "react";
import { SectionHeadline, ResumeDate } from "./ResumeUtils.js";

/**
 * The basics of a resume require the following
 *  - name # Nagee Elghassein
 *  - email # isthisnagee@gmail.com
 * The rest is optional
 *  - label # Programmer
 *  - picture # link/to/img.jpg
 *  - phone # (916) 450-1958
 *  - website # https://isthisnagee.github.io
 *  - summary # a bunch of markdown (!)
 *  - location
 *     - city  # Toronto
 *     - region # Ontario
 *     - country (optional) # Canada
 *     - address (optional) # 22 College St
 *     - postalCode (otpional) # 95630
 *  - profiles (list of:)
 *     - network # Twitter
 *     - username # isthisnagee
 *     - url # https://twitter.com/isthisnagee
 */

function EducationContent({ educations, ...props }) {
  // educations is a list of education
  const educationSections = educations.map((education, id) => {
    return <Education key={id} order={id + 1} education={education} />;
  });

  return (
    <section className="cf">
      {SectionHeadline("Education")}
      {educationSections}
    </section>
  );
}

function Education({ education, order, ...props }) {
  // let's make sure this is valid
  const valid =
    education &&
    education.institution &&
    education.studyType &&
    education.startDate;
  if (!valid) return false;

  const area = education.area ? <li>{education.area}</li> : false;
  const gpa = education.gpa ? <li>GPA: {education.gpa}</li> : false;
  const date = (
    <ResumeDate startDate={education.startDate} endDate={education.endDate} />
  );
  const courses = education.courses
    ? education.courses.map((course, id) => <li key={id}>{course}</li>)
    : false;

  return (
    <div className="">
      {date}
      <div className="h-100 fl">
        <h3 className="f4 tc tl-ns">
          {education.institution}, {education.studyType}
        </h3>
        <ul className="list">
          {area}
          {gpa}
          <li>
            Courses
            <ul>
              {courses}
            </ul>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default EducationContent;
