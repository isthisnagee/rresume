import React from "react";
import { SectionHeadline } from "./ResumeUtils.js";

export default ReferenceContent;

function ReferenceContent({ references, ...props }) {
  if (!references) return false;
  const referenceContent = references.map((reference, id) => {
    return <Reference key={id} reference={reference} />;
  });
  return (
    <div>
      {SectionHeadline("References")}
      {referenceContent}
    </div>
  );
}

function Reference({ reference, ...props }) {
  const valid = reference && reference.name && reference.reference;
  if (!valid) return false;

  return (
    <div className="pa4">
      <blockquote className="ml0 mt0 pl4 black-90 bl bw2">
        <p className="f6 lh-copy measure mt0">
          {reference.reference}
        </p>
        <cite className="ttu tracked fs-normal">―{reference.name}</cite>
      </blockquote>
    </div>
  );
}
