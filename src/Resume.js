import React from "react";
import Header from "./Header.js";
import EducationContent from "./Education.js";
import WorkContent from "./Work.js";
import VolunteerContent from "./Volunteer.js";
import AwardContent from "./AwardContent.js";
import PublicationContent from "./PublicationContent.js";
import SkillContent from "./SkillContent.js";
import LanguageContent from "./LanguageContent.js";
import ReferenceContent from "./ReferenceContent.js";
import InterestContent from "./InterestContent.js";
import "./Resume.css";
import "./tachyons.css";

function Resume({ data, ...props }) {
  return (
    <div>
      <nav id="top" className="Resume-nav">
        <ul>
          <li className="Resume-nav__item">
            <a href="#education">Education</a>
          </li>
          <li className="Resume-nav__item"><a href="#work">Work</a></li>
          <li className="Resume-nav__item">
            <a href="#volunteer">Volunteer</a>
          </li>
          <li className="Resume-nav__item"><a href="#awards">Awards</a></li>
          <li className="Resume-nav__item">
            <a href="#publications">Publications</a>{" "}
          </li>
          <li className="Resume-nav__item"><a href="#skills">Skills</a></li>
          <li className="Resume-nav__item">
            <a href="#languages">Languages</a>
          </li>
          <li className="Resume-nav__item">
            <a href="#references">References</a>
          </li>
          <li className="Resume-nav__item">
            <a href="#interests">Interests</a>
          </li>
        </ul>
      </nav>
      <article className="Resume">
        <a id="basics" />
        <Header basics={data.basics} />
        <a id="education" /><a className="toTop" href="#top" />
        <EducationContent educations={data.education} />
        <a id="work" /><a className="toTop" href="#top" />
        <WorkContent experience={data.work} />
        <a id="volunteer" /><a className="toTop" href="#top" />
        <VolunteerContent experience={data.volunteer} />
        <a id="awards" /><a className="toTop" href="#top" />
        <AwardContent awards={data.awards} />
        <a id="publications" /><a className="toTop" href="#top" />
        <PublicationContent publications={data.publications} />
        <a id="skills" /><a className="toTop" href="#top" />
        <SkillContent skills={data.skills} />
        <a id="languages" /><a className="toTop" href="#top" />
        <LanguageContent languages={data.languages} />
        <a id="references" /><a className="toTop" href="#top" />
        <ReferenceContent references={data.references} />
        <a id="interests" /><a className="toTop" href="#top" />
        <InterestContent interests={data.interests} />
      </article>
    </div>
  );
}

export default Resume;
